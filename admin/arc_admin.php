<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
use \Intervolga\Arc\ArcConfig;
use \Intervolga\Arc\ArcFormula;
use \Intervolga\Arc\MeasurmentTable;
use \Intervolga\Arc;
use \Intervolga\Arc\ArcDataProvider;
use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
Loc::loadLanguageFile(__FILE__);

\CModule::IncludeModule("iblock");
if(!\CModule::IncludeModule("catalog")) return;
if(!\CModule::IncludeModule('main')) return;
$msg = "";
$dataProvider = new ArcDataProvider();
?>

<?
/* -------- CONTROLLER -------- */
if($_POST['start_new_section'] === 'true')
{
    $secNum = ArcConfig::getSectionNumber();
    $secNum++;
    ArcConfig::setSectionNumber($secNum);

    $msg .= GetMessage("ARC_MODULE_DONE");
}

if($_POST['reset_tables'] === 'true')
{
    MeasurmentTable::destroyTable();
    MeasurmentTable::createTable();

    $msg .= GetMessage("ARC_MODULE_DONE");
}

if($_POST['reset_sections'] === 'true')
{
    ArcConfig::setSectionNumber(ArcConfig::$default_section_number);

    $msg .= GetMessage("ARC_MODULE_DONE");
}

if($_POST['switch'] === 'true' || $_POST['switch'] === 'false')
{
    if($_POST['switch'] === 'false')
    {
        ArcConfig::switchOff();
    }
    else
    {
        ArcConfig::switchOn();
    }
    $msg .= GetMessage("ARC_MODULE_DONE");
}
//расчет рейтинга товаров
if($_POST['calc'] === 'true')
{
    $catalog = $dataProvider->getCatalog();
    $n = $dataProvider->getGoodsCount();
    $formula = new ArcFormula(6, $n);

    //проходим по каждому товару
    while($item = $catalog->Fetch())
    {
        $id = $item['ID'];
        $section = ArcConfig::getSectionNumber();

        //получает статистику товара
        $mt_db = MeasurmentTable::getList(
            array(
            'select' => array("ASSESSMENT", "POSITION", "PID"),
            'filter' => array(
                "=SECTION" => $section,
                "=PID" => $id
                ),
            )
        );
        $sum = 0.0;
        //расчет суммы рейтига товара
        while($mt_item = $mt_db->Fetch())
        {
            $sum += $formula->g($mt_item['ASSESSMENT'], $mt_item['POSITION']);
        }
        //устанавливает рейтинг определенному товару
        $dataProvider->setSort($id, $sum * 10);
        echo "ID:" . $id . ", RATING:" . $sum * 10 . '<br>';
    }

    $msg .= GetMessage("ARC_MODULE_DONE");
}

if($_POST['reset_rating'] === 'true')
{
    $СElement = new \CIBlockElement();
    $catalog = $СElement->getList(
        $pid,
        array('=IBLOCK_TYPE' => "catalog")
    );
    while($item = $catalog->Fetch())
    {
        $dataProvider->setSort($item['ID'], 0);
    }

    $msg .= GetMessage("ARC_MODULE_DONE");
}

?>

<? /* -------- VIEW -------- */ ?>

<style>
    .arc_button
    {
        width: 150px;
        height: 40px;
    }
</style>

    <!-- List of sections -->
<div>
    <? echo GetMessage("ARC_MODULE_ENABLED"); ?> : <? echo ArcConfig::isSwitchedOn() == true ? GetMessage("ARC_MODULE_YES") : GetMessage("ARC_MODULE_NO") ?> <br/>

    <? $sectionNumber = ArcConfig::getSectionNumber(); ?>

    <? echo GetMessage("ARC_MODULE_SECTION_INDEX"); ?>: <? echo $sectionNumber == ArcConfig::$default_section_number ? "0" : $sectionNumber ?> <br/>

    <? echo GetMessage("ARC_MODULE_SECTION_LIST"); ?>:<br/>
    <table border="1">
        <tr>
            <td><? echo GetMessage("ARC_MODULE_LIST_INDEX"); ?></td>
            <td><? echo GetMessage("ARC_MODULE_LIST_DATETIME"); ?></td>
        </tr>
    <?for($i = 0; $i <= $sectionNumber; ++$i) {?>
        <tr>
            <td><? echo $i; ?></td>
            <td><? echo ArcConfig::getSectionStartDate($i) ?></td>
        </tr>
    <?}?>
    </table>
    <br/>

</div>
<br>
<div>
    <form action="/bitrix/admin/arc_admin.php" method="post">
        <input class="arc_button" type="hidden" name="reset_sections" value="true">
        <input class="arc_button" type="submit" value="<? echo GetMessage("ARC_MODULE_SECTION_RESET"); ?>"/>
    </form>
</div>

<div> <!-- Section control -->
    <form action="/bitrix/admin/arc_admin.php" method="post">
        <input type="hidden" name="start_new_section" value="true">
        <input class="arc_button" type="submit" value="<? echo GetMessage("ARC_MODULE_SECTION_NEW"); ?>"/>
    </form>
</div>

<div>
    <form action="/bitrix/admin/arc_admin.php" method="post">
        <input type="hidden" name="reset_tables" value="true">
        <input class="arc_button" type="submit" value="<? echo GetMessage("ARC_MODULE_CLEAR_TABLES"); ?>"/>
    </form>
</div>

<div>
    <form action="/bitrix/admin/arc_admin.php" method="post">
        <input type="hidden" name="switch" value="true">
        <input class="arc_button" type="submit" value="<? echo GetMessage("ARC_MODULE_SWITCH_ON"); ?>"/>
    </form>
</div>

<div>
    <form action="/bitrix/admin/arc_admin.php" method="post">
        <input type="hidden" name="switch" value="false">
        <input class="arc_button" type="submit" value="<? echo GetMessage("ARC_MODULE_SWITCH_OFF"); ?>"/>
    </form>
</div>

<div>
    <form action="/bitrix/admin/arc_admin.php" method="post">
        <input type="hidden" name="calc" value="true">
        <input class="arc_button" type="submit" value="<? echo GetMessage("ARC_MODULE_CALC"); ?>"/>
    </form>
</div>

<div>
    <form action="/bitrix/admin/arc_admin.php" method="post">
        <input type="hidden" name="reset_rating" value="true">
        <input class="arc_button" type="submit" value="<? echo GetMessage("ARC_MODULE_RESET_RATING"); ?>"/>
    </form>
</div>

<?
echo $msg;
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin_after.php");
?>