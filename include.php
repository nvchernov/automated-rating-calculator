<?
use \Bitrix\Main\Page\Asset;

CJSCore::Init(array("jquery"));
Asset::getInstance()->addJs("https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.0/jquery.cookie.min.js");
Asset::getInstance()->addJs("/bitrix/js/arc_spy.js");

\Bitrix\Main\Loader::registerAutoLoadClasses(
	"intervolga.arc",
	array(
        "\\Intervolga\\Arc\\MeasurmentTable" => "lib/MeasurmentTable.php",
        "\\Intervolga\\Arc\\ArcConfig" => "lib/ArcConfig.php",
		"\\Intervolga\\Arc\\ArcDataProvider" => "lib/ArcDataProvider.php",
		"\\Intervolga\\Arc\\ArcFormula" => "lib/ArcFormula.php",
		"\\Intervolga\\Arc\\ArcEventsHandler" => "lib/ArcEventsHandler.php",

	)
);