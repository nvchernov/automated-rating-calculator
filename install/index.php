<?
IncludeModuleLangFile(__FILE__);

\CModule::IncludeModule('intervolga.arc');

class intervolga_arc extends CModule
{
    const MODULE_ID = 'intervolga.arc';

    var $MODULE_ID = 'intervolga.arc';
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $MODULE_CSS;

    function __construct()
    {
        $arModuleVersion = array();

        include(dirname(__FILE__) . "/version.php");

        $this->MODULE_VERSION = $arModuleVersion["VERSION"];
        $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];

        $this->MODULE_NAME = GetMessage('ARC_MODULE_NAME');
        $this->MODULE_DESCRIPTION = GetMessage('ARC_MODULE_DESCRIPTION');
    }

    function InstallDB()
    {
        \Intervolga\Arc\MeasurmentTable::createTable();
    }

    function UnInstallDB()
    {
        \Intervolga\Arc\MeasurmentTable::destroyTable();
    }

    function InstallFiles()
    {
        CopyDirFiles(dirname(__FILE__)."/js", $_SERVER["DOCUMENT_ROOT"]."/bitrix/js", true);
        CopyDirFiles(dirname(__FILE__)."/admin", $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin", true);
        CopyDirFiles(
            dirname(__FILE__)."/services/ajax/arc/",
            $_SERVER["DOCUMENT_ROOT"]."/bitrix/services/ajax/arc",
            true);
	}

    function UnInstallFiles()
    {
        unlink($_SERVER["DOCUMENT_ROOT"]."/bitrix/js/arc_spy.js");
        unlink($_SERVER["DOCUMENT_ROOT"]."/bitrix/admin/arc_admin.php");
        unlink($_SERVER["DOCUMENT_ROOT"]."/bitrix/services/ajax/arc/arc_ajax.php");
    }

    function RegisterEvents()
    {

    }

    function UnRegisterEvents()
    {

    }

    function DoInstall()
    {
        RegisterModule($this->MODULE_ID);
        \CModule::IncludeModule('intervolga.arc');
        $this->InstallFiles();
        $this->InstallDB();
        $this->RegisterEvents();
    }

    function DoUninstall()
    {
        $this->UnInstallDB();
        $this->UnInstallFiles();
        $this->UnRegisterEvents();
        \Intervolga\Arc\ArcConfig::setSectionNumber(
            \Intervolga\Arc\ArcConfig::$default_section_number);
        UnRegisterModule($this->MODULE_ID);
    }

}
