window.onload = function() {

    //--------------- definitions ---------------

    //url to send products via ajax
    var ajax_url = "/bitrix/services/ajax/arc/arc_ajax.php";
    var user_cookie_name = "ARC_COOKIE_USER";
    var click_cookie_name = "ARC_CLICKED_PID";
    var user_cookie_lifetime = 15; //minutes
    var pid_attr_name = "data-arc-pid";

    var page_number = get_url_variable("PAGEN_1");
    page_number = page_number > 1 ? page_number : 1;

    //--------------- user management ---------------

    var generate_user_id = function () {
        return "" + Math.round(Math.random() * 2 * 1000 * 1000);
    };

    var is_user_new = function () {
        return typeof $.cookie(user_cookie_name) == 'undefined';
    };

    var reset_user = function (user_id) {
        var date = new Date();
        date.setTime(date.getTime() + (user_cookie_lifetime * 60 * 1000));

        $.cookie(
            user_cookie_name,
            user_id,
            {
                path: '/',
                expires: date
            }
        );
    };

    var get_user_id = function () {
        var user_id;

        if (is_user_new()) {
            user_id = generate_user_id();
            reset_user(user_id);
        }

        user_id = $.cookie(user_cookie_name);

        return user_id;
    };

    var user_id = get_user_id();

    reset_user(user_id);

    function get_url_variable(variable) {
        var query = window.location.search.substring(1);
        var vars = query.split("&");
        for (var i=0;i<vars.length;i++) {
            var pair = vars[i].split("=");
            if(pair[0] == variable){
                return pair[1];
            }
        }
        return false;
    }

    var pid_list = []; //include all product ids that was viewed
    var tmp_pos = 0;
    var $links_dom = $('[' + pid_attr_name + ']');

    $links_dom.each(function (i, v) {
        var $v = $(v);
        var pid = $v.attr(pid_attr_name);
        if(pid_list.indexOf(pid) == -1) //element not found
        {
            pid_list.push(pid);
            ++tmp_pos;
        }
    });

    $links_dom.each(function (i, v) {
        var $v = $(v);
        var pid = $v.attr(pid_attr_name);
        $v.click(function(){
            var date = new Date();
            date.setTime(date.getTime() + (user_cookie_lifetime * 60 * 1000));

            $.cookie(
                click_cookie_name,
                JSON.stringify({
                    pid: pid,
                    user_id: user_id,
                    page_number: page_number,
                    position: pid_list.indexOf(pid) + 1
                }),
                {
                    path: '/',
                    expires: date
                }
            );
        });
    });

    //--------------- prepare data ---------------

    $.ajax({
        url: ajax_url,
        method: "POST",
        data: { catalog_data: { pid_list: pid_list, user_id: user_id, page_number: page_number } },
        success: function(data, textStatus, jqXHR) {

        },
        error: function (jqXHR, textStatus, errorThrown) {

        }
    });

    var user_click_data = JSON.parse($.cookie(click_cookie_name));
    if( typeof user_click_data != 'undefined' )
    {
        $.ajax({
            url: ajax_url,
            method: "POST",
            data: { click_data: user_click_data },
            success:  function(data, textStatus, jqXHR) {
                $.removeCookie(click_cookie_name, { path: '/' }); //удаляет куки в случае успешной отправки данных
            },
            error: function (jqXHR, textStatus, errorThrown) {

            }
        });
    }

};