<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php';

if (!CModule::IncludeModule('intervolga.arc')) return;
if (!CModule::IncludeModule("sale")) return;

use Intervolga\Arc;
use Intervolga\Arc\MeasurmentTable;
use Intervolga\Arc\ArcConfig;
use Bitrix\Main\DB\SqlExpression;
use Bitrix\Main\DB;
use Bitrix\Main\Type;
use Intervolga\Arc\ArcDataProvider;

if(ArcConfig::isSwitchedOn() == false)
{
    echo "module intervolga.arc disabled";
    die();
}

//клиент прислал данные со списка товаров
if(isset($_GET['catalog_data']))
{
    $arc_data = $_GET['catalog_data'];

    $pid_list = $arc_data['pid_list'];
    $user_id = (int)$arc_data['user_id'];
    $page_number = (int)$arc_data['page_number'];

    $section = ArcConfig::getSectionNumber();
    $positions_count = count($pid_list);

    $position = 0;

    foreach($pid_list as $item)
    {
        MeasurmentTable::add(
            array(
                "PID" => $item,
                "UID" => $user_id,
                "SECTION" => $section,
                "POSITION" => $positions_count * ($page_number - 1) + $position + 1,
                "ASSESSMENT" => -1,
            )
        );
        ++$position;
    }
}

if(isset($_GET['click_data']))
{
    $arc_data = $_GET['click_data'];
    $pid = (int)$arc_data['pid'];
    $user_id = (int)$arc_data['user_id'];
    $page_number = (int)$arc_data['page_number'];

    $section = ArcConfig::getSectionNumber();
    $positions_count = 10;

    $position = (int)$arc_data['position'];

    MeasurmentTable::add(
        array(
            "PID" => $pid,
            "UID" => $user_id,
            "SECTION" => $section,
            "POSITION" => $positions_count * ($page_number - 1) + $position,
            "ASSESSMENT" => 1,
        )
    );
}
require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/epilog_after.php';