<?php namespace Intervolga\Arc;

/**
 * Class ArcFormula - позвляет расчитать рейтинг для определенного товара
 * @package Intervolga\Arc
 */
class ArcFormula
{
    private $alpha = 6.0;
    private $n = 10;

    function __construct($alpha = 6.0, $n = 10)
    {
        $this->$alpha = $alpha;
        $this->n = $n;
    }

    function g($E, $L)
    {
        if($E == 1)
        {
            return 1/(1 + exp(-1 * $this->alpha * (2 * $L / $this->n - 1)));
        }
        else if($E == -1)
        {
            return 1/(1 + exp(-1 * $this->alpha * (2 * $L / $this->n - 1))) - 1;
        }
        return 0;
    }
}
