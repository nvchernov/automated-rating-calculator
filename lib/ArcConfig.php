<?php namespace Intervolga\Arc;

if(!\CModule::IncludeModule('main')) die();
use \Bitrix\Main;
use \Bitrix\Main\Config;
use \Bitrix\Main\Config\Option;

/**
 * Class ArcConfig - конфигурация модуля ранжирования
 * @package Intervolga\Arc
 */
class ArcConfig {
    public static $module_id = "intervolga.arc";
    public static $default_section_number = -1;
    private static $section_option_name = "section_number";
    private static $switch_option_name = "section_switch";
    private static $option_date_name = "section_date";

    /**
     * Получить номер замера
     * @return int номер замера
     */
    public static function getSectionNumber()
    {
        return \Bitrix\Main\Config\Option::get(self::$module_id, self::$section_option_name, self::$default_section_number);
    }

    /**
     * Задать номер замера
     * @param $val - номер замера
     */
    public static function setSectionNumber($val)
    {
        \Bitrix\Main\Config\Option::set(self::$module_id, self::$section_option_name, $val);
        self::setSectionStartDate(
            $val,
            Main\Type\DateTime::createFromPhp(new \DateTime('NOW'))->format("Y-m-d H:i:s")
        );
    }

    /**
     * Получить дату начала замера
     * @param $section - номер замера
     * @return string|NULL
     */
    public static function getSectionStartDate($section)
    {
        return \Bitrix\Main\Config\Option::get(
            self::$module_id,
            self::$option_date_name . $section,
            NULL);
    }

    /**
     * Установить дату начала замера
     * @param $section - номер замера
     * @param $val - дата
     */
    public static function setSectionStartDate($section, $date)
    {
        \Bitrix\Main\Config\Option::set(
            self::$module_id,
            self::$option_date_name . $section,
            $date);
    }

    /**
     * Выключить сбор данных
     */
    public static function switchOff()
    {
        \Bitrix\Main\Config\Option::set(self::$module_id, self::$switch_option_name, 0);
    }

    /**
     * Включить сбор данных
     */
    public static function switchOn()
    {
        \Bitrix\Main\Config\Option::set(self::$module_id, self::$switch_option_name, 1);
    }

    /**
     * Получить состояние сбора данных
     * @return bool, true - сборданных включен, false - сбор данных выключен
     * @throws Main\ArgumentNullException
     */
    public static function isSwitchedOn()
    {
        if(\Bitrix\Main\Config\Option::get(self::$module_id, self::$switch_option_name, 0) == 1 &&
            self::getSectionNumber() != self::$default_section_number)
            return true;
        else
            return false;
    }
}