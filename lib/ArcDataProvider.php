<?php namespace Intervolga\Arc;

if(!\CModule::IncludeModule('main')) return;
use \Bitrix\Main;
use \Bitrix\Main\Config;
use \Bitrix\Main\Config\Option;
use Intervolga\Arc;
use Intervolga\Arc\MeasurmentTable;
use Intervolga\Arc\ArcConfig;
use Bitrix\Main\DB\SqlExpression;
use Bitrix\Main\DB;
use Bitrix\Main\Type;
\CModule::IncludeModule("iblock");
class ArcDataProvider
{
    private $СElement;
    function __construct()
    {
        $this->СElement = new \CIBlockElement();
    }

    /**
     * Устанавливает значение сортировки товару
     * @param $pid - ид товара
     * @param $value - значение поля сортировки
     */
    function setSort($pid, $value)
    {
        $this->СElement->Update(
            $pid,
            array('SORT' => $value)
        );
    }

    /**
     * Количество товаров
     */
    function getGoodsCount()
    {
        $catalog = $this->СElement->getList(
            array(),
            array('=IBLOCK_TYPE' => "catalog")
        );
        return $catalog->SelectedRowsCount();
    }

    function getCatalog()
    {
        $catalog = $this->СElement->getList(
            array(),
            array('=IBLOCK_TYPE' => "catalog")
        );
        return $catalog;
    }

}