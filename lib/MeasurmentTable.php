<?php namespace Intervolga\Arc;

use Bitrix\Main\Entity;
class MeasurmentTable extends Entity\DataManager {

    public static function getFilePath()
    {
        return __FILE__;
    }

    public static function getTableName()
    {
        return 'arc_measurment';
    }

    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
            ),
            'PID' => array(
                'data_type' => 'integer'
            ),
            'UID' => array(
                'data_type' => 'integer'
            ),
            'SECTION' => array(
                'data_type' => 'integer'
            ),
            'ASSESSMENT' => array(
                'data_type' => 'integer'
            ),
            'POSITION' => array(
                'data_type' => 'integer'
            )
        );
    }

    public static function createTable()
    {
        $sql_query = '
            CREATE TABLE IF NOT EXISTS ' . self::getTableName() . '(
                ID              INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
                PID             INT NOT NULL,
                UID             INT NOT NULL,
                SECTION         INT NOT NULL DEFAULT 0,
                POSITION        INT NOT NULL DEFAULT 0,
                ASSESSMENT      INT NOT NULL DEFAULT -1
            );';
        global $DB;
        return $DB->Query($sql_query, false);
    }

    public static function destroyTable()
    {
        $sql_query = 'DROP TABLE IF EXISTS ' . self::getTableName();

        global $DB;
        return $DB->Query($sql_query, false);
    }

}
